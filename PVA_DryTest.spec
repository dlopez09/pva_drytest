# -*- mode: python -*-

import sys
sys.setrecursionlimit(5000)


from kivy_deps import sdl2, glew

block_cipher = None


a = Analysis(['PVA_DryTest.py'],
             pathex=['C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\PVA_Dry'],
             binaries=[],
             datas=[],
             hiddenimports=['win32file', 'win32timezone'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas += [('TesterGUI.kv', 'C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\PVA_Dry\\TesterGUI.kv', 'DATA')]
exe = EXE(pyz, Tree('C:\\Users\\dlopez\\Desktop\\Work\\IPV\\NewCodeRepo\\PVA_Dry'),
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
          name='PVA_DryTest_v0.9.4',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=False )
